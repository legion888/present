﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Logic
{
    public class PresentService
    {
        public double Price(Present present)
        {
            double _pricePresent = 0;
            List<AbstractCandy> _candy=present.GetCandies();
            List<int> _quantity=present.GetQuantity();

            for (int i = 0; i < _candy.Count; i++)
            {
                _pricePresent += _quantity[i] * _candy[i].GetPrice();
            }
            return _pricePresent;
        }



    }
}
