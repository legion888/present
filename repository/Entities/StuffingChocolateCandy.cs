﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class StuffingChocolateCandy : ChocolateCandy
    {
        private string _stuffing;

        public StuffingChocolateCandy()
        {
            
        }

        public StuffingChocolateCandy(string stuffing)
        {
            _stuffing = stuffing;
        }

        public string GetStuffing()
        {
            return _stuffing;
        }

        public void SetStuffing(string stuffing)
        {
            _stuffing = stuffing;
        }

    }
}
