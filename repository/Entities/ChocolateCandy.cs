﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ChocolateCandy : AbstractCandy
    {
        private ChocolateType _chocolateType;

        public ChocolateCandy()
        {
            
        }

        public ChocolateCandy(ChocolateType chocolateType)
        {
            _chocolateType = chocolateType;
        }


        public ChocolateType GetChocolateType()
        {
            return _chocolateType;
        }

        public void SetChocolateType(ChocolateType chocolateType)
        {
            _chocolateType = chocolateType;
        }



    }
}
