﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    class DataHolder
    {
        public string name { get; set; }
        public double price { get; set; }
        public double weight { get; set; }
        public ChocolateType chocolateType { get; set; }
        public string stuffing { get; set; }
        public FlavorType flavor { get; set; }
    }
}
