﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class BuilderFactory
    {
        public FlavorType flavorType(int Index)
        { 
            FlavorType flavorType;
            switch (Index)
                { 
                    case 0:
                        flavorType = FlavorType.Caramel;
                        break;
                    case 1:
                        flavorType = FlavorType.Dragee;
                        break;
                    case 2:
                        flavorType = FlavorType.Barshmallows;
                        break;
                    case 3:
                        flavorType = FlavorType.Jelly;
                        break;
                    default:
                        flavorType = FlavorType.Iris;
                        break;
                }
            return flavorType;
        }

        public int IndexflavorType(FlavorType flavorType)
        { 
            int index;
            switch (flavorType)
                {
                    case FlavorType.Caramel:
                        index = 0;
                        break;
                    case FlavorType.Dragee:
                        index = 1;
                        break;
                    case FlavorType.Barshmallows:
                        index = 2;
                        break;
                    case FlavorType.Jelly:
                        index = 3;
                        break;
                    default:
                        index = 4;
                        break;
                }
            return index;
        }







        public ChocolateType chocolateType(int Index)
        {
            ChocolateType chocolateType;
            switch (Index)
            {
                case 0:
                    chocolateType = ChocolateType.Black;
                    break;
                case 1:
                    chocolateType = ChocolateType.White;
                    break;
                default:
                    chocolateType = ChocolateType.Milk;
                    break;
            }
            return chocolateType;
        }



        public int IndexChocolateType(ChocolateType chocolateType)
        { 
            int index;
            switch (chocolateType)
                {
                    case ChocolateType.Black:
                        index = 0;
                        break;
                    case ChocolateType.White:
                        index = 1;
                        break;
                    default:
                        index = 2;
                        break;
                }
            return index;
        }
    }
}
