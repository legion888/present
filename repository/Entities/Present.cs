﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Present
    {
        private List<AbstractCandy> _candies;
        private List<int> _quantity;

        public Present()
        {
            _candies = new List<AbstractCandy>();
            _quantity = new List<int>();
        }

        public void DisplayCandy(AbstractCandy candy, int quantity)
        {
            _candies.Add(candy);
            _quantity.Add(quantity);
        }

        public void EditingCandy(AbstractCandy candy, int quantity, int index)
        {
            _candies[index] = candy;
            _quantity[index] = quantity;
        }

        public List<AbstractCandy> GetCandies()
        {
            return _candies;
        }

        public List<int> GetQuantity()
        {
            return _quantity;
        }

        public void delete(int number)
        {
            _candies.RemoveAt(number);
            _quantity.RemoveAt(number);
        }

    }
}
