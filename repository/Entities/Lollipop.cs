﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Lollipop : AbstractCandy
    {
        private FlavorType _flavor;  //вкус

        public Lollipop()
        {
            
        }

        public Lollipop(FlavorType flavor)
        {
            _flavor = flavor;
        }

        public FlavorType GetFlavor()
        {
            return _flavor;
        }

        public void SetFlavor(FlavorType flavor)
        {
            _flavor = flavor;
        }


    }
}
