﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class AbstractCandy
    {
        private double _weight;  //вес
        private double _price;   //цена
        private string _name;    //название

        public AbstractCandy()
        {
            
        }

        public AbstractCandy(double weight, double price, string name)
        {
            _weight = weight;
            _price=price;
            _name = name;
        }

        public double GetWeight()
        {
            return _weight;
        }
        public void SetWeight(double weight)
        {
            _weight = weight;
        }


        public double GetPrice()
        {
            return _price;
        }
        public void SetPrice(double price)
        {
            _price = price;
        }


        public string GetName()
        {
            return _name;
        }
        public void SetName(string name)
        {
            _name = name;
        }
    }
}
