﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using Logic;

namespace NewYearPresent
{
    class Program
    {
        static void Main(string[] args)
        {

            PresentFactory presentFactory=new PresentFactory();
            Present present = presentFactory.CreatePresent();

            Printer printer = new Printer();
            printer.PrintPresentContent(present);
            printer.PrintPresentPrice(present);
        }
    }
}


//Новогодний подарок. Определить иерархию конфет и прочих сладостей. 
//Создать несколько объектов-конфет. Собрать детский подарок с определением его веса.