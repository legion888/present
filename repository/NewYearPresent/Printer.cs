﻿using Entities;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NewYearPresent
{
    class Printer
    {
        public void PrintPresentPrice(Present present)
        {
            PresentService presentService = new PresentService();
            double price = presentService.Price(present);
            Console.WriteLine("Цена подарка = " + price);
        }

        public void PrintPresentContent(Present present)
        {
            List<AbstractCandy> candies = present.GetCandies();
            List<int> quantity = present.GetQuantity();

            for (int i = 0; i < candies.Count; i++)
            {
                AbstractCandy candy = candies[i];
                int count = quantity[i];
                Console.WriteLine(candy.GetName() + ": " + count + " штук, по " + candy.GetPrice());
            }
        
        
        }
    }
}
