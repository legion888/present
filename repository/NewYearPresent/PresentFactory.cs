﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace NewYearPresent
{
    class PresentFactory
    {
        public Present CreatePresent()
        {
            StuffingChocolateCandy stuffingChocolateCandy = new StuffingChocolateCandy("Карамель");//начинка
            stuffingChocolateCandy.SetName("Мерси");
            stuffingChocolateCandy.SetPrice(5);
            stuffingChocolateCandy.SetWeight(25);
            stuffingChocolateCandy.SetChocolateType(ChocolateType.Milk);


            Lollipop lollipop = new Lollipop();
            lollipop.SetName("Барбариска");
            lollipop.SetPrice(1);
            lollipop.SetWeight(10);
            lollipop.SetFlavor(FlavorType.Jelly);

            Present present = new Present();
            present.DisplayCandy(stuffingChocolateCandy, 25);
            present.DisplayCandy(lollipop, 15);


            return present;
        }


    }
}
