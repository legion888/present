﻿using Entities;
using Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PresentDesktop
{
    public partial class Candyes : Form
    {
        private Present _present = new Present();

        public Present GetPresent()
        {
            return _present;
        }

        public void SetPresent(Present present)
        {
            _present = present;
        }

        public Candyes()
        {
            InitializeComponent();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonAddCandy_Click(object sender, EventArgs e)
        {
            AddEditForm form = new AddEditForm();
            form.Owner = this; //Передаём вновь созданной форме её владельца.
            form.Show();
        }

        private void buttonEditCandy_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                AddEditForm form = new AddEditForm(_present.GetCandies()[listBox1.SelectedIndex], _present.GetQuantity()[listBox1.SelectedIndex], listBox1.SelectedIndex);
                form.Owner = this; //Передаём вновь созданной форме её владельца.
                form.Show();
            }
        }

        public void displayCandyAndPrice()
        {
            listBox1.Items.Clear();
            for (int i = 0; i < _present.GetCandies().Count; i++)
            {
                listBox1.Items.Add(_present.GetCandies()[i].GetName().ToString() + " - " + _present.GetQuantity()[i].ToString() + " штук");
            }
            PresentService presentPrice = new PresentService();
            double price = presentPrice.Price(_present);
            Price.Text = "Цена подарка " + price;
        }

        private void buttonDeleteCandy_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                _present.delete(listBox1.SelectedIndex);
            }
            displayCandyAndPrice();
        }

        private void Candyes_Load(object sender, EventArgs e)
        {
            StuffingChocolateCandy stuffingChocolateCandy = new StuffingChocolateCandy("Карамель");//начинка

            stuffingChocolateCandy.SetName("Мерси");
            stuffingChocolateCandy.SetPrice(5);
            stuffingChocolateCandy.SetWeight(25);
            stuffingChocolateCandy.SetChocolateType(ChocolateType.Milk);

            Lollipop lollipop = new Lollipop();
            lollipop.SetName("Барбариска");
            lollipop.SetPrice(1);
            lollipop.SetWeight(10);
            lollipop.SetFlavor(FlavorType.Jelly);

            
            _present.DisplayCandy(stuffingChocolateCandy, 25);
            _present.DisplayCandy(lollipop, 15);

            displayCandyAndPrice();
        }
    }
}
