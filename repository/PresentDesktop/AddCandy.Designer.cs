﻿namespace PresentDesktop
{
    partial class AddEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBoxTypeCandy = new System.Windows.Forms.ComboBox();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.comboBoxСhocolateType = new System.Windows.Forms.ComboBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.labelName = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.labelWeight = new System.Windows.Forms.Label();
            this.labelTypeCandy = new System.Windows.Forms.Label();
            this.labelСhocolateType = new System.Windows.Forms.Label();
            this.comboBoxFlavorType = new System.Windows.Forms.ComboBox();
            this.labelFlavorType = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxStuffing = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 514);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Ok_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(352, 514);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // comboBoxTypeCandy
            // 
            this.comboBoxTypeCandy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTypeCandy.FormattingEnabled = true;
            this.comboBoxTypeCandy.Items.AddRange(new object[] {
            "ChocolateCandy",
            "Lollipop"});
            this.comboBoxTypeCandy.Location = new System.Drawing.Point(207, 138);
            this.comboBoxTypeCandy.Name = "comboBoxTypeCandy";
            this.comboBoxTypeCandy.Size = new System.Drawing.Size(144, 24);
            this.comboBoxTypeCandy.TabIndex = 2;
            this.comboBoxTypeCandy.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTypeCandy_SelectionChangeCommitted);
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Location = new System.Drawing.Point(207, 110);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(144, 22);
            this.textBoxWeight.TabIndex = 3;
            this.textBoxWeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxWeight_KeyPress);
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(207, 82);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(144, 22);
            this.textBoxPrice.TabIndex = 4;
            this.textBoxPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPrice_KeyPress);
            // 
            // comboBoxСhocolateType
            // 
            this.comboBoxСhocolateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxСhocolateType.Enabled = false;
            this.comboBoxСhocolateType.FormattingEnabled = true;
            this.comboBoxСhocolateType.Items.AddRange(new object[] {
            "Black",
            "White",
            "Milk"});
            this.comboBoxСhocolateType.Location = new System.Drawing.Point(207, 172);
            this.comboBoxСhocolateType.Name = "comboBoxСhocolateType";
            this.comboBoxСhocolateType.Size = new System.Drawing.Size(144, 24);
            this.comboBoxСhocolateType.TabIndex = 5;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(207, 54);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(144, 22);
            this.textBoxName.TabIndex = 6;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(41, 58);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(45, 17);
            this.labelName.TabIndex = 7;
            this.labelName.Text = "Name";
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(41, 85);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(40, 17);
            this.labelPrice.TabIndex = 8;
            this.labelPrice.Text = "Price";
            // 
            // labelWeight
            // 
            this.labelWeight.AutoSize = true;
            this.labelWeight.Location = new System.Drawing.Point(41, 115);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(52, 17);
            this.labelWeight.TabIndex = 9;
            this.labelWeight.Text = "Weight";
            // 
            // labelTypeCandy
            // 
            this.labelTypeCandy.AutoSize = true;
            this.labelTypeCandy.Location = new System.Drawing.Point(40, 141);
            this.labelTypeCandy.Name = "labelTypeCandy";
            this.labelTypeCandy.Size = new System.Drawing.Size(80, 17);
            this.labelTypeCandy.TabIndex = 10;
            this.labelTypeCandy.Text = "TypeCandy";
            // 
            // labelСhocolateType
            // 
            this.labelСhocolateType.AutoSize = true;
            this.labelСhocolateType.Location = new System.Drawing.Point(41, 175);
            this.labelСhocolateType.Name = "labelСhocolateType";
            this.labelСhocolateType.Size = new System.Drawing.Size(103, 17);
            this.labelСhocolateType.TabIndex = 11;
            this.labelСhocolateType.Text = "СhocolateType";
            // 
            // comboBoxFlavorType
            // 
            this.comboBoxFlavorType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFlavorType.Enabled = false;
            this.comboBoxFlavorType.FormattingEnabled = true;
            this.comboBoxFlavorType.Items.AddRange(new object[] {
            "Caramel",
            "Dragee",
            "Barshmallows",
            "Jelly",
            "Iris"});
            this.comboBoxFlavorType.Location = new System.Drawing.Point(207, 203);
            this.comboBoxFlavorType.Name = "comboBoxFlavorType";
            this.comboBoxFlavorType.Size = new System.Drawing.Size(144, 24);
            this.comboBoxFlavorType.TabIndex = 12;
            // 
            // labelFlavorType
            // 
            this.labelFlavorType.AutoSize = true;
            this.labelFlavorType.Location = new System.Drawing.Point(41, 206);
            this.labelFlavorType.Name = "labelFlavorType";
            this.labelFlavorType.Size = new System.Drawing.Size(79, 17);
            this.labelFlavorType.TabIndex = 13;
            this.labelFlavorType.Text = "FlavorType";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 239);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Stuffing";
            // 
            // textBoxStuffing
            // 
            this.textBoxStuffing.Enabled = false;
            this.textBoxStuffing.Location = new System.Drawing.Point(207, 239);
            this.textBoxStuffing.Name = "textBoxStuffing";
            this.textBoxStuffing.Size = new System.Drawing.Size(144, 22);
            this.textBoxStuffing.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(43, 269);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 16;
            this.label2.Text = "Quantity";
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Location = new System.Drawing.Point(207, 269);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(144, 22);
            this.textBoxQuantity.TabIndex = 17;
            this.textBoxQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxQuantity_KeyPress);
            // 
            // AddEditForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 549);
            this.Controls.Add(this.textBoxQuantity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxStuffing);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelFlavorType);
            this.Controls.Add(this.comboBoxFlavorType);
            this.Controls.Add(this.labelСhocolateType);
            this.Controls.Add(this.labelTypeCandy);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.labelPrice);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.comboBoxСhocolateType);
            this.Controls.Add(this.textBoxPrice);
            this.Controls.Add(this.textBoxWeight);
            this.Controls.Add(this.comboBoxTypeCandy);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "AddEditForm";
            this.Text = "AddCandy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBoxTypeCandy;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.ComboBox comboBoxСhocolateType;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.Label labelTypeCandy;
        private System.Windows.Forms.Label labelСhocolateType;
        private System.Windows.Forms.ComboBox comboBoxFlavorType;
        private System.Windows.Forms.Label labelFlavorType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxStuffing;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxQuantity;
    }
}