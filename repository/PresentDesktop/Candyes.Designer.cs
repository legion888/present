﻿namespace PresentDesktop
{
    partial class Candyes
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonAddCandy = new System.Windows.Forms.Button();
            this.buttonEditCandy = new System.Windows.Forms.Button();
            this.buttonDeleteCandy = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.Price = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonAddCandy
            // 
            this.buttonAddCandy.Location = new System.Drawing.Point(11, 13);
            this.buttonAddCandy.Name = "buttonAddCandy";
            this.buttonAddCandy.Size = new System.Drawing.Size(109, 31);
            this.buttonAddCandy.TabIndex = 0;
            this.buttonAddCandy.Text = "add";
            this.buttonAddCandy.UseVisualStyleBackColor = true;
            this.buttonAddCandy.Click += new System.EventHandler(this.buttonAddCandy_Click);
            // 
            // buttonEditCandy
            // 
            this.buttonEditCandy.Location = new System.Drawing.Point(11, 66);
            this.buttonEditCandy.Name = "buttonEditCandy";
            this.buttonEditCandy.Size = new System.Drawing.Size(109, 31);
            this.buttonEditCandy.TabIndex = 1;
            this.buttonEditCandy.Text = "edit";
            this.buttonEditCandy.UseVisualStyleBackColor = true;
            this.buttonEditCandy.Click += new System.EventHandler(this.buttonEditCandy_Click);
            // 
            // buttonDeleteCandy
            // 
            this.buttonDeleteCandy.Location = new System.Drawing.Point(11, 118);
            this.buttonDeleteCandy.Name = "buttonDeleteCandy";
            this.buttonDeleteCandy.Size = new System.Drawing.Size(109, 31);
            this.buttonDeleteCandy.TabIndex = 2;
            this.buttonDeleteCandy.Text = "delete";
            this.buttonDeleteCandy.UseVisualStyleBackColor = true;
            this.buttonDeleteCandy.Click += new System.EventHandler(this.buttonDeleteCandy_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 16;
            this.listBox1.Location = new System.Drawing.Point(126, 13);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(261, 372);
            this.listBox1.TabIndex = 3;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // Price
            // 
            this.Price.AutoSize = true;
            this.Price.Location = new System.Drawing.Point(13, 406);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(80, 17);
            this.Price.TabIndex = 4;
            this.Price.Text = "TotalPrice: ";
            // 
            // Candyes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 435);
            this.Controls.Add(this.Price);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.buttonDeleteCandy);
            this.Controls.Add(this.buttonEditCandy);
            this.Controls.Add(this.buttonAddCandy);
            this.Name = "Candyes";
            this.Text = "Candyes";
            this.Load += new System.EventHandler(this.Candyes_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonAddCandy;
        private System.Windows.Forms.Button buttonEditCandy;
        private System.Windows.Forms.Button buttonDeleteCandy;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label Price;
    }
}

