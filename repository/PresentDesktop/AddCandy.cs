﻿using Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace PresentDesktop
{
    public partial class AddEditForm : Form
    {
        public AddEditForm()
        {
            InitializeComponent();
        }


        int _index = -1;
        public AddEditForm(AbstractCandy candy, int quantity, int index) : this()
        {
            _index = index;
            if (candy != null)
            {
                BuilderFactory factory = new BuilderFactory();

                textBoxName.Text = candy.GetName();
                textBoxPrice.Text = candy.GetPrice().ToString();
                textBoxWeight.Text = candy.GetWeight().ToString();
                textBoxQuantity.Text = quantity.ToString();
                if (candy is StuffingChocolateCandy)
                {
                    comboBoxTypeCandy.SelectedIndex = 0;
                    StuffingChocolateCandy chocolateCandy = (StuffingChocolateCandy)candy;
                    ChocolateType chocolateType = chocolateCandy.GetChocolateType();
                    comboBoxСhocolateType.SelectedIndex = (int)chocolateType;
                    textBoxStuffing.Text = chocolateCandy.GetStuffing();

                    comboBoxСhocolateType.Enabled = true;
                    comboBoxFlavorType.Enabled = false;
                    textBoxStuffing.Enabled = true;
                }
                if (candy is Lollipop)
                {
                    comboBoxTypeCandy.SelectedIndex = 1;
                    Lollipop lollipopCandy = (Lollipop)candy;
                    FlavorType flavor = lollipopCandy.GetFlavor();
                    comboBoxFlavorType.SelectedIndex = (int)flavor;

                    comboBoxСhocolateType.Enabled = false;
                    comboBoxFlavorType.Enabled = true;
                    textBoxStuffing.Enabled = false;
                }
            }
        }


        private void Ok_Click(object sender, EventArgs e)
        {
            Candyes frm = (Candyes)this.Owner;
            Present present = frm.GetPresent();

            if (comboBoxTypeCandy.SelectedIndex == 0)
            {
                StuffingChocolateCandy newСandy = new StuffingChocolateCandy();//начинка
                BuilderFactory factory = new BuilderFactory();

                newСandy.SetName(textBoxName.Text);
                newСandy.SetPrice(Convert.ToDouble(textBoxPrice.Text));
                newСandy.SetWeight(Convert.ToDouble(textBoxWeight.Text));
                newСandy.SetStuffing(textBoxStuffing.Text);

                ChocolateType type=factory.chocolateType(comboBoxСhocolateType.SelectedIndex);
                newСandy.SetChocolateType(type);

                int quantity = Convert.ToInt32(textBoxQuantity.Text);
                    if (_index == -1)
                        present.DisplayCandy(newСandy, quantity);
                    else
                        present.EditingCandy(newСandy, quantity, _index);
            }
            if (comboBoxTypeCandy.SelectedIndex == 1)
            {
                Lollipop newСandy = new Lollipop();
                BuilderFactory factory = new BuilderFactory();

                newСandy.SetName(textBoxName.Text);
                newСandy.SetPrice(Convert.ToDouble(textBoxPrice.Text));
                newСandy.SetWeight(Convert.ToDouble(textBoxWeight.Text));

                FlavorType type= factory.flavorType(comboBoxFlavorType.SelectedIndex);
                newСandy.SetFlavor(type);

                int quantity = Convert.ToInt32(textBoxQuantity.Text);
                    if (_index == -1)
                        present.DisplayCandy(newСandy, quantity);
                    else
                        present.EditingCandy(newСandy, quantity, _index);
            }
            frm.SetPresent(present);
            frm.displayCandyAndPrice();
            this.Close();

        }

        private void comboBoxTypeCandy_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxTypeCandy.SelectedIndex == 0)
            {
                comboBoxСhocolateType.Enabled = true;
                comboBoxFlavorType.Enabled = false;
                textBoxStuffing.Enabled = true;
            }
            if (comboBoxTypeCandy.SelectedIndex == 1)
            {
                comboBoxСhocolateType.Enabled = false;
                comboBoxFlavorType.Enabled = true;
                textBoxStuffing.Enabled = false;
            }
        }

        private void textBoxWeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                    e.Handled = true;
            }
        }

        private void textBoxPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                    e.Handled = true;
            }
        }

        private void textBoxQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                if ((e.KeyChar <= 47 || e.KeyChar >= 58) && e.KeyChar != 8)
                    e.Handled = true;
            }
        }
    }
}
